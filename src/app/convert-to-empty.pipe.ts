import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertToEmpty'
})
export class ConvertToEmptyPipe implements PipeTransform {

  transform(value: string, character:string): string {
    return value.split(character).join('');
  }

}
