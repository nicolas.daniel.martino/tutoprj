import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fils',
  templateUrl: './fils.component.html',
  styleUrls: ['./fils.component.scss']
})
export class FilsComponent implements OnInit {
  @Input() userName: string;
  @Input() firstName: string;
  constructor() { }

  ngOnInit() {
  }

}
