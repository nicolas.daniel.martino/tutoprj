import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Fils2wayComponent } from './fils2way.component';

describe('Fils2wayComponent', () => {
  let component: Fils2wayComponent;
  let fixture: ComponentFixture<Fils2wayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Fils2wayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Fils2wayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
