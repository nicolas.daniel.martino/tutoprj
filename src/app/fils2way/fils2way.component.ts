import { Component, OnInit, Output, OnChanges, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-fils2way',
  templateUrl: './fils2way.component.html',
  styleUrls: ['./fils2way.component.scss']
})
export class Fils2wayComponent implements OnInit, OnChanges {
  @Input() a: string;
  @Input() b: string;
  @Output() ratingA: EventEmitter<string> = new EventEmitter<string>();
  @Output() ratingB: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnChanges() { }

  ngOnInit() {
  }


  NotifyA() {
    // console.log('this.a :', this.a);
    this.ratingA.emit(this.a);

  }
  NotifyB() {
    // console.log('this.b :', this.b);
    this.ratingB.emit(this.b);
  }
}
