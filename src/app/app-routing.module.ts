import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogComponent } from './catalog/catalog.component';
import { HomeComponent } from './home/home.component';
import { identifierModuleUrl } from '@angular/compiler';
import { CataloguedetailComponent } from './cataloguedetail/cataloguedetail.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },

  { path: 'catalogue', component: CatalogComponent },
  { path: 'catalogue/:id', component: CataloguedetailComponent },
  { path: 'home', redirectTo: 'home', pathMatch: 'full' },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
