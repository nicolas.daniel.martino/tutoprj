import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Catalog, Product } from '../model/catalog';



@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  private productUrl = "../../assets/api/products/products.json";
  private postUrl = "https://jsonplaceholder.typicode.com/posts";

  constructor(private _http: HttpClient) { }

  getCatalog() {
    this._http.get(this.productUrl).subscribe(data => localStorage.setItem("products", JSON.stringify(data)));
    return JSON.parse(localStorage.getItem("products")) as Product[];
  }

  // getCatalogByIndex(index:number) {
  //   return JSON.parse(localStorage.getItem("products"))[index] as Product;
  // }

  getElementNextTo(obj, n) {
    let data = JSON.parse(localStorage.getItem("products")) as Product[];
    let precedentCurId = null;
    let curId = null; // evealuates to -1
    for (let i = 0; i < data.length; i++) {
      if (data[i].productId == obj.productId) {
        curId = i;
        break;
      }
      precedentCurId = i;
    }

    let res;
    //fallback to beginning of the array or the end if go above or below its boudaries
    console.log('object :', precedentCurId, curId);
    if (curId === 0 && n < 0) res = data.length - 1;
    else if (curId === data.length - 1 && n > 0) res = 0;
    else res = curId + n;

    return data[res];
  }

  getCatalogById(productId: number) {
    let data: Product = JSON.parse(localStorage.getItem("products")).filter(it => {
      return it.productId == productId
    }) as Product;
    return data[0];
  }

  // getPosts() {
  //    this._http.get(this.postUrl).subscribe(data=>
  //     this.listdeProduitsCache = data as Product[]);
  //   return this.listdeProduitsCache;
  // }
}
