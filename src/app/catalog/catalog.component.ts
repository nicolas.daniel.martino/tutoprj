import { Component, OnInit } from '@angular/core';
import { CatalogService } from '../catalog/catalog.service';
import { Posts, Product } from '../model/catalog';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
  showPictures:boolean = true;
  
  searchText:string;
  listPosts:Array<Posts> = new Array<Posts>();
  listProduct:Array<Product> = new Array<Product>();

  ngOnInit() {
  }

  /**
   *
   */
  constructor(private _serviceCatalog: CatalogService) {
      this.listProduct = this._serviceCatalog.getCatalog();
      // this._serviceCatalog.getPosts().toPromise().then(data =>(Posts) data);
  }
  tooglePicView(){
    this.showPictures = !this.showPictures;
  }
}
