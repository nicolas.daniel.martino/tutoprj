import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Router } from '@angular/router';
import {CatalogService} from '../catalog/catalog.service';
import { Product } from '../model/catalog';

@Component({
  selector: 'app-cataloguedetail',
  templateUrl: './cataloguedetail.component.html',
  styleUrls: ['./cataloguedetail.component.scss']
})
export class CataloguedetailComponent implements OnInit {
selectedId : number;
selectedProduct : Product;

  constructor(private _route: ActivatedRoute, private _router: Router, private _catalog: CatalogService) {
    // if(this.selectedId ==null || this.selectedId == undefined) this._router.navigate(['/catalogue']);
    console.log('this._route :', this._route.snapshot.paramMap.get('id'));
    this.selectedId = Number(this._route.snapshot.paramMap.get('id')); 

    this._route.paramMap.subscribe((paramsData : ParamMap) => {
      console.log("Paramètre récupérés : =>", paramsData);
      this.selectedId = parseInt(paramsData.get('id'))
    })

    console.log('object :', _catalog.getCatalog());
    this.selectedProduct = _catalog.getCatalogById(this.selectedId);
    console.log('selectedProduct :', this.selectedProduct);
  }
  onBack(): void {
    this._router.navigate(['/catalogue']);
  }
  goPrevious():void{
    this.selectedProduct = this._catalog.getElementNextTo(this.selectedProduct, -1);
    this._router.navigate(['catalogue', this.selectedProduct.productId]);
  }
  goNext():void{
    this.selectedProduct = this._catalog.getElementNextTo(this.selectedProduct, 1);
    this._router.navigate(['catalogue', this.selectedProduct.productId]);
  }
  ngOnInit() {
  }

}
