import { Component, OnInit } from '@angular/core';
import People from '../model/people';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})

export class FormComponent implements OnInit {
  tempUser: People;

  peoples: Array<People>;
  addUser() {
    console.log('this.tempUser.anyEmptyField:', this.tempUser.anyEmptyField());
    this.peoples.push(this.tempUser);
    console.log('tempUser :', this.peoples);
    this.tempUser = new People();
  }
  constructor() {
    this.tempUser = new People();
    this.peoples = new Array<People>();
  }

  ngOnInit() {
  }

}
