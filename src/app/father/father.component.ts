import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-father',
  templateUrl: './father.component.html',
  styleUrls: ['./father.component.scss']
})
export class FatherComponent implements OnInit {
  userName: string = "Martial";
  firstName: string = "Bush";

  a: string;
  b: string;

  constructor() { }

  ngOnInit() {
  }
  onRatingAClicked(eventValue: string) {
    // console.log('a :', eventValue);
    this.a = eventValue;
  }
  onRatingBClicked(eventValue: string) {
    // console.log('b :', eventValue);
    this.b = eventValue;
  }
}
