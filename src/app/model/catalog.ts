export class Catalog {

}


export class Posts {
    userId: number;
    id: number;
    title: string;
    body: string;
}




export class Product {
    productId: number;
    productName: string;
    productCode: string;
    releaseDate: string;
    description: string;
    price: number;
    starRating: number;
    imageUrl: string;
}
