import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { FormsModule } from '@angular/forms';
import { FatherComponent } from './father/father.component';
import { FilsComponent } from './fils/fils.component';
import { Fils2wayComponent } from './fils2way/fils2way.component';
import { ConvertToSpacePipe } from './convert-to-space.pipe';
import { ConvertToEmptyPipe } from './convert-to-empty.pipe';
import { CatalogComponent } from './catalog/catalog.component';
import { HttpClientModule } from '@angular/common/http';
import { CatalogService } from './catalog/catalog.service';
import { StarRatingComponent } from './star-rating/star-rating.component';
import { FilterPipe } from './filter.pipe';
import { NavComponent } from './nav/nav.component';
import { HomeComponent } from './home/home.component';
import { CataloguedetailComponent } from './cataloguedetail/cataloguedetail.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    FatherComponent,
    FilsComponent,
    Fils2wayComponent,
    ConvertToSpacePipe,
    ConvertToEmptyPipe,
    CatalogComponent,
    StarRatingComponent,
    FilterPipe,
    NavComponent,
    HomeComponent,
    CataloguedetailComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,

  ],
  providers: [CatalogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
